#!/bin/bash -xe
# Tool to reset (code and database) in egov-education
MY_PWD=${PWD##*/}          # to assign to a variable
MY_PWD=${MY_PWD:-/}        # to correct for the case where PWD=/
if [ "$MY_PWD" != egov-education ]
then
    echo 1>&2 "Current working dir must be egov-education"
    exit 1
fi
rm -rf *
git checkout .
git pull
cd backoffice-sql
PGPASSWORD=egoveducation psql -h localhost egoveducation egoveducation <<EOF
DROP OWNED BY egoveducation;
\i lib/sql/install/egoveducation.sql
EOF
cd ../backoffice-dsu
npm install
cd api
npm run start:dev
