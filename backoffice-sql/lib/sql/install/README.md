# sql/install

The scripts under this install folder are supposed to run only once, when the database is setup for the 1st time.

## Files

```sh
setup.sql - create user and databse egoveducation (to run as PostgreSQL superuser - typically postgres)
egoveducation.sql - create and populate egoveducation database (to run as egoveducation user)
egoveducation_run.sh - to be used from the top level Dockerfile. Do not run manually.
```