// 2022-11-09 jpsl - based on https://medium.com/@amcdnl/version-stamping-your-app-with-the-angular-cli-d563284bb94d
const { gitDescribeSync } = require('git-describe');
const { version } = require('../package.json');
const { resolve, relative } = require('path');
const fs = require('fs');

const gitInfo = gitDescribeSync({
    dirtyMark: false,
    dirtySemver: false
});

gitInfo.version = version;

const apiHubIndexHtmlFile = resolve(__dirname, '..', 'apihub-root', 'index.html');
fs.readFile(apiHubIndexHtmlFile, 'utf8', function (err, data) {
    if (err)
        return console.log(err);
    var result = data.replace(
        /<!-- VERSION_START -->.*<!-- VERSION_END -->/g,
        '<!-- VERSION_START -->'+gitInfo.version+"-"+gitInfo.hash+'<!-- VERSION_END -->');
    fs.writeFile(apiHubIndexHtmlFile, result, 'utf8', function (err) {
        if (err) return console.log(err);
    });
});
console.log(`Wrote version info ${gitInfo.raw} to ${relative(resolve(__dirname, '..'), apiHubIndexHtmlFile)}`);