import { Module } from '@nestjs/common'; 
import { EGovEducationModule } from './egoveducation/egoveducation.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [EGovEducationModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
