import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { LocalStrategy } from './local.strategy';
import { JwtStrategy } from './jwt.strategy';
import { EGovEducationModule } from '../egoveducation/egoveducation.module';

@Module({
  imports: [
    EGovEducationModule,
    PassportModule,
    JwtModule.register({
       secret: 'a-secret-that-should-be-moved-to-a-config-in-db',
       signOptions: { expiresIn: '2h' },
    })
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy],
})
export class AuthModule { }
