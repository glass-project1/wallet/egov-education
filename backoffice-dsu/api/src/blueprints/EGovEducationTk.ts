import type {Constructor, DataProperties} from "@glass-project1/dsu-blueprint";
import type {IEGovEducation, ToolkitInjectables} from "@glass-project1/glass-toolkit";

/**
 * {@link RenderableDSUBlueprint} decorated Builtin Class representing an EGov Education
 *
 * @class EGovEducation
 * @extends EvidenceBlueprint
 */
export function EGovEducationTk(injectables: ToolkitInjectables): Constructor<IEGovEducation>
export function EGovEducationTk(injectables: ToolkitInjectables, data: DataProperties<IEGovEducation>): IEGovEducation
export function EGovEducationTk(injectables: ToolkitInjectables, data?: DataProperties<IEGovEducation>): Constructor<IEGovEducation> | IEGovEducation {

    @injectables.RenderableDSUBlueprint("glass-education-records-tk", {}, undefined, injectables.KeySSIType.SEED, true)
    @injectables.RenderableEvidence("glass-education-records-tk", "../components/components", undefined, false)
    class EGovEducationTk extends injectables.EvidenceBlueprint {

        @injectables.uiprop("id-number")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        id?: string = undefined;

        @injectables.uiprop("given-name")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        givenName?: string = undefined;

        @injectables.uiprop("surname")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        surname?: string = undefined;

        @injectables.uiprop("birth-date")
        @injectables.required()
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        birthDate?: Date = undefined;

        @injectables.uiprop("nationality")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        nationality?: string = undefined;

        @injectables.uiprop("sex")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        gender?: string = undefined;

        @injectables.uiprop("primary-school-name")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        primarySchoolName?: string = undefined;

        @injectables.uiprop("primary-school-degree-issue-date")
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        primarySchoolDegreeIssueDate?: Date = undefined;

        @injectables.uiprop("primary-school-grade")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        primarySchoolGrade?: string = undefined;

        @injectables.uiprop("lower-secondary-school-name")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        lowerSecondarySchoolName?: string = undefined;

        @injectables.uiprop("lower-secondary-school-degree-issue-date")
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        lowerSecondarySchoolDegreeIssueDate?: Date = undefined;

        @injectables.uiprop("lower-secondary-school-grade")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        lowerSecondarySchoolGrade?: string = undefined;

        @injectables.uiprop("higher-secondary-school-name")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        higherSecondarySchoolName?: string = undefined;

        @injectables.uiprop("higher-secondary-school-degree-issue-date")
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        higherSecondarySchoolDegreeIssueDate?: Date = undefined;

        @injectables.uiprop("higher-secondary-school-grade")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        higherSecondarySchoolGrade?: string = undefined;

        @injectables.uiprop("tertiary-school-name")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        tertiarySchoolName?: string = undefined;

        @injectables.uiprop("tertiary-degree-issue-date")
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        tertiaryDegreeIssueDate?: Date = undefined;

        @injectables.uiprop("tertiary-school-grade")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        tertiarySchoolGrade?: string = undefined;

        @injectables.uiprop("tertiary-school-type")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        tertiarySchoolType?: string = undefined;

        @injectables.uiprop("ects-credits")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        ectsCredits?: string = undefined;


        constructor(eGovEducation?: EGovEducationTk | {}) {
            super(eGovEducation);
            injectables.constructFromBlueprint<EGovEducationTk>(this, eGovEducation);
        }
    }

    if (data)
        return new EGovEducationTk(data);

    return EGovEducationTk;
}