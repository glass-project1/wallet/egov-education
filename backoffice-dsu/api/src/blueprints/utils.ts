import {Constructor} from "@glass-project1/dsu-blueprint";
import {eGovEducationInjectables} from "./constants";
import type {EGovEducationInjectables} from "./types";

type EGovEducationBlueprintFunction<T> = (injectables: EGovEducationInjectables) => Constructor<T>;

export function getBlueprint<T>(blueprintName: string, countryCode: string): EGovEducationBlueprintFunction<T> | undefined {
    countryCode = countryCode.toLowerCase();
    countryCode = countryCode.charAt(0).toUpperCase() + countryCode.slice(1);
    return eGovEducationInjectables.blueprints[blueprintName + countryCode];
}