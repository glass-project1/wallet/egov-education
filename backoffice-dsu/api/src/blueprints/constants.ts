import {getBlueprintInjectables, setBlueprintInjectables} from "@glass-project1/dsu-blueprint";
import {toolkitInjectables} from "@glass-project1/glass-toolkit";
import {EGovEducationDAppGr} from "./EGovEducationDAppGr";
import {EGovEducationDAppPt} from "./EGovEducationDAppPt";
import {EGovEducationDAppTk} from "./EGovEducationDAppTk";
import {EGovEducationGr} from "./EGovEducationGr";
import {EGovEducationPt} from "./EGovEducationPt";
import {EGovEducationTk} from "./EGovEducationTk";

import type {EGovEducationInjectables} from "./types";

export const eGovEducationInjectables: EGovEducationInjectables = {
    ...toolkitInjectables,
    blueprints: {
        ...toolkitInjectables.blueprints,

        EGovEducationGr,
        EGovEducationPt,
        EGovEducationTk,

        EGovEducationDAppGr,
        EGovEducationDAppPt,
        EGovEducationDAppTk,
    }
}

export function getEGovEducationInjectables(): EGovEducationInjectables {
    setBlueprintInjectables(eGovEducationInjectables, true);
    return getBlueprintInjectables() as EGovEducationInjectables;
}