import type {Constructor} from "@glass-project1/dsu-blueprint";
import type {IEGovEducation, IEGovEducationDApp, IGlassDID} from "@glass-project1/glass-toolkit";
import type {DSUDatabase, DSUEnclave, DSUSecurityContext, EnvironmentDefinition} from "@glass-project1/opendsu-types";
import type {EGovEducationInjectables} from "./types";

/**
 * {@link DSUBlueprint} decorated Builtin Class representing the {@link EGovEducationDApp}
 *
 * Note that it contains an {@link EGovEducationDApp} under 'education'
 *
 * @class EGovEducationDApp
 * @extends DBModel
 */
export function EGovEducationDAppGr(injectables: EGovEducationInjectables): Constructor<IEGovEducationDApp>
export function EGovEducationDAppGr(injectables: EGovEducationInjectables, data: Record<string, any>): IEGovEducationDApp
export function EGovEducationDAppGr(injectables: EGovEducationInjectables, data?: Record<string, any>): Constructor<IEGovEducationDApp> | IEGovEducationDApp {

    const eGovEducationClass = injectables.blueprints.EGovEducationGr(injectables);

    @injectables.DSUBlueprint(undefined, injectables.KeySSIType.SEED)
    class EGovEducationDAppGr extends injectables.DBModel {

        @injectables.dsuMixinBlueprint(injectables.blueprints.EGovEducationGr, injectables, true, true)
        education?: IEGovEducation = undefined;

        @injectables.signedDID(undefined)
        did?: IGlassDID = undefined;

        @injectables.enclave()
        enclave?: DSUEnclave = undefined;

        @injectables.environment()
        environment?: EnvironmentDefinition = undefined;

        @injectables.walletDB()
        db?: DSUDatabase = undefined;

        @injectables.securityContext()
        sc?: DSUSecurityContext = undefined;

        constructor(eGovEducationdApp?: EGovEducationDAppGr | {}) {
            super();
            injectables.constructFromBlueprint(this, eGovEducationdApp);
            this.did = new injectables.GlassDID(this.did);
            this.education = new eGovEducationClass(this.education);
        }
    }

    if (data)
        return new EGovEducationDAppGr(data);

    return EGovEducationDAppGr;
}