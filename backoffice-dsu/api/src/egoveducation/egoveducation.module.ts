import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppResourceController } from './appresource.controller';
import { EgovEducationController } from './egoveducation.controller';
import { EGovEducationService } from './egoveducation.service';
import { AppUserService } from './appuser.service';

@Module({
  imports: [HttpModule,
    TypeOrmModule.forRoot({
    "name": "default",
    "type": "postgres",
    "host": ( process.env.EGOVEDUCATIONDB_HOST || "localhost" ),
    "port": ( process.env.EGOVEDUCATIONDB_PORT ? parseInt(process.env.EGOVEDUCATIONDB_PORT) : 5432 ),
    "username": "egoveducation",
    "password": "egoveducation",
    "database": "egoveducation",
    "entities": [
      "dist/egoveducation/*.entity.js"
    ],
    "synchronize": false,
    "logging": true
  })],
  controllers: [
    AppResourceController,
    EgovEducationController,
  ],
  providers: [
    EGovEducationService,
    AppUserService
  ],
  exports: [
    AppUserService
  ],
})
export class EGovEducationModule { }
