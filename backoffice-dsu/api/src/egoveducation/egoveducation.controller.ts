import { Connection } from "typeorm";
import { Controller, Get, Put, Param, Body, Post, UseGuards, Query, NotFoundException } from '@nestjs/common';
import { AuthGuard } from "@nestjs/passport";
import { ApiBearerAuth, ApiOperation, ApiTags, ApiParam, getSchemaPath, ApiExtraModels, ApiOkResponse } from "@nestjs/swagger";
import { EGovEducationEntity } from './egoveducation.entity';
import { EGovEducationService } from './egoveducation.service';
import { EGovEducationQuery, EGovEducationQueryValidator } from "./egoveducationquery.validator";
import { EGovEducationRepository } from "./egoveducation.repository";
import { PaginatedDto } from "../paginated.dto";



 
@ApiExtraModels(PaginatedDto)
@ApiTags('EgovEducation')
//@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('/egoveducation/egoveducation')
export class EgovEducationController {

    private eGovEducationRepository: EGovEducationRepository;

    constructor(
        private connection: Connection,
        private egovEducationService: EGovEducationService
    ) {
        this.eGovEducationRepository = this.connection.getCustomRepository(EGovEducationRepository);
    }

    @Get()
    @ApiOperation({summary: "Search for EGovEducation based on a query, with paginated results."})
    @ApiOkResponse({
        schema: {
            allOf: [
                { $ref: getSchemaPath(PaginatedDto) },
                {
                    properties: {
                        results: {
                            type: 'array',
                            items: { $ref: getSchemaPath(EGovEducationEntity) },
                        },
                    },
                },
            ],
        },
    })
    async search(@Query(EGovEducationQueryValidator) eGovEducationQuery: EGovEducationQuery): Promise<PaginatedDto<EGovEducationQuery, EGovEducationEntity>> {
        console.log("egoveducation.controller.search... query=", eGovEducationQuery);
        const page = await this.eGovEducationRepository.search(eGovEducationQuery);
        console.log("egoveducation.controller.search results =", page);
        return page;
    }

    @Get(":id")
    @ApiOperation({ summary: 'Get one EGovEducation record' })
    @ApiParam({ name: 'id', type: String, description: "Exact match of primary key column EGovEducation.id" })
    @ApiOkResponse({
        type: EGovEducationEntity
    })
    async findOne(@Param() params): Promise<EGovEducationEntity> {
        console.log("egoveducation.controller.findOne... id=", params.id);
        let eGovEducation = await EGovEducationEntity.findOne(params.id);
        if (!eGovEducation) throw new NotFoundException(`Not found EGovEducation.id="${params.id}"`);
        console.log("egoveducation.controller.findOne egoveducation =", eGovEducation);
        return eGovEducation;
    }

    /* don't allow  update PUT */

    @Post(":id")
    @ApiOperation({ summary: 'Create one wallet for a given ID' })
    @ApiParam({ name: 'id', type: String })
    @ApiOkResponse({
        status: 201,
        type: EGovEducationEntity
    })
    async createWallet(@Param() params, @Body() body: any): Promise<EGovEducationEntity> {
        console.log(`egoveducation.controller.post/${params.id}...body`, body);

        const eGovEducation = await this.egovEducationService.createWallet(params.id);
    
        console.log("egoveducation.controller.post/${params.id} DB connection closed, egoveducation =", eGovEducation);
        return eGovEducation;
    }


    @Get("/setup/onetime")
    @ApiOperation({ summary: 'Setup a wallet with a DID for this government agency. No need to invoke explicitely, as it will be invoked internally on the 1st use.' })
    @ApiOkResponse({ description: "The returned string is the agency's DID. (Even if invoked more than once, the agency DID is always the same)."})
    async setup() : Promise<string> {
        console.log(`egoveducation.controller.setup`);

        const agencyDid = await this.egovEducationService.getAgencyDID();
    
        console.log("egoveducation.controller.setup =", agencyDid);
        return agencyDid.getIdentifier();
    }

}

