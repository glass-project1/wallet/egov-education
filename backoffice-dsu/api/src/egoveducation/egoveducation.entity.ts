import { BaseEntity, Column, Entity, PrimaryColumn } from "typeorm";
import { ApiProperty } from '@nestjs/swagger';


@Entity("egoveducation")
export class EGovEducationEntity extends BaseEntity {  

    @ApiProperty({ description: "CCV_00022_IDnumber Mandatory Citizen Identification Number" })
    @PrimaryColumn()
    id: string;  

    @ApiProperty({ description: "CCV_00002_name Given names" })
    @Column({ name: "givenname" })
    givenName: string;

    @ApiProperty({ description: "CCV_00003_Surname Surnames" })
    @Column()
    surname: string;

    @ApiProperty({ description: "CCV_00004_DateofBirth Birth date" })
    @Column({ name: "birthdate" })
    birthDate: Date;

    @ApiProperty({ description: "CCV_00012_Nationality Nationality" })
    @Column({ name: "nationality" })
    nationality: string;

    @ApiProperty({ description: "CCV_00013_Sex Gender" })
    @Column({ name: "gender" })
    gender: string;

    @ApiProperty({ description: "CCV_00052_primarySchoolName Primary school name" })
    @Column({ name: "primaryschoolname" })
    primarySchoolName: string;
      
    @ApiProperty({ description: "CCV_00053_primarySchoolDegreeIssueDate Primary school degree issue date" })
    @Column({ name: "primaryschooldegreeissuedate" })
    primarySchoolDegreeIssueDate: Date;

    @ApiProperty({ description: "CCV_00054_primarySchoolGrade Lower school grade" })
    @Column({ name: "primaryschoolgrade" })
    primarySchoolGrade: string;

    @ApiProperty({ description: "CCV_00057_lowerSecondarySchoolName Lower school name" })
    @Column({ name: "lowersecondaryschoolname" })
    lowerSecondarySchoolName: string;
      
    @ApiProperty({ description: "CCV_00058_lowerSecondarySchoolDegreeIssueDate Lower school degree issue date" })
    @Column({ name: "lowersecondaryschooldegreeissuedate" })
    lowerSecondarySchoolDegreeIssueDate: Date;

    @ApiProperty({ description: "CCV_00059_lowerSecondarySchoolGrade Lower school grade" })
    @Column({ name: "lowersecondaryschoolgrade" })
    lowerSecondarySchoolGrade: string;

    @ApiProperty({ description: "CCV_00061_higherSecondarySchoolName Higher school name" })
    @Column({ name: "highersecondaryschoolname" })
    higherSecondarySchoolName: string;
      
    @ApiProperty({ description: "CCV_00062_higherSecondarySchoolDegreeIssueDate Higher school degree issue date" })
    @Column({ name: "highersecondaryschooldegreeissuedate" })
    higherSecondarySchoolDegreeIssueDate: Date;

    @ApiProperty({ description: "CCV_00063_higherSecondarySchoolGrade Higher school grade" })
    @Column({ name: "highersecondaryschoolgrade" })
    higherSecondarySchoolGrade: string;
    
    @ApiProperty({ description: "CCV_00065_tertiarySchoolName Tertiary school name" })
    @Column({ name: "tertiaryschoolname" })
    tertiarySchoolName: string;
      
    @ApiProperty({ description: "CCV_00066_tertiaryDegreeIssueDate Tertiary school degree issue date" })
    @Column({ name: "tertiarydegreeissuedate" })
    tertiaryDegreeIssueDate: Date;

    @ApiProperty({ description: "CCV_00067_tertiarySchoolGrade Tertiary school grade" })
    @Column({ name: "tertiaryschoolgrade" })
    tertiarySchoolGrade: string;

    @ApiProperty({ description: "CCV_00068_tertiarySchoolType Tertiary schooltype" })
    @Column({ name: "tertiaryschooltype" })
    tertiarySchoolType: string;
    
    @ApiProperty({ description: "CCV_00069_ectsCredits ECTS Credits" })
    @Column({ name: "ectscredits" })
    ectsCredits: string;

    @ApiProperty({ description: "A JSON containing the wallet DSU seed keySSI. Non-null if the wallet has already been created. See AppResource.key=education.dapp.keyTemplate for an example of such JSON.", required: false })
    @Column({ name: "walletkeyssi" })
    walletKeySSI: string;

    @ApiProperty({ description: "ISO-3166-1 alpha-2 two letter country code to which this record belongs. Normally, all the records in the same agency belong to the same country, but this column exists for developers to be allowed to mix records from distinct countries.", required: false })
    @Column({ name: "country" })
    country: string;
}
