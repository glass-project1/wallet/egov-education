import { newE2EPage } from '@stencil/core/testing';

describe('glass-education-records-tk', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-education-records-tk></glass-education-records-tk>');

    const element = await page.find('glass-education-records-tk');
    expect(element).toHaveClass('hydrated');
  });
});
