# glass-education-records



<!-- Auto Generated Below -->


## Properties

| Property                               | Attribute                                   | Description                                    | Type             | Default                        |
| -------------------------------------- | ------------------------------------------- | ---------------------------------------------- | ---------------- | ------------------------------ |
| `birthDate`                            | `birth-date`                                | Insurance Expiration date                      | `Date \| string` | `"1990/01/01"`                 |
| `ectsCredits`                          | `ects-credits`                              | The tertiary  school degree issue grade        | `string`         | `"10"`                         |
| `gender`                               | `gender`                                    | The sex                                        | `string`         | `"Female"`                     |
| `givenName`                            | `given-name`                                | The givenname of the id                        | `string`         | `"Ana Cardoso"`                |
| `higherSecondarySchoolDegreeIssueDate` | `higher-secondary-school-degree-issue-date` | The higher secondary school degree issue date  | `Date \| string` | `"2003/12/30"`                 |
| `higherSecondarySchoolGrade`           | `higher-secondary-school-grade`             | The higher secondary school degree issue grade | `string`         | `"8"`                          |
| `higherSecondarySchoolName`            | `higher-secondary-school-name`              | The higher secondary school name               | `string`         | `"2003/12/30"`                 |
| `idNumber`                             | `id-number`                                 | The id-number of the id                        | `string`         | `"GB 14 WXYZ 205623 25648978"` |
| `lowerSecondarySchoolDegreeIssueDate`  | `lower-secondary-school-degree-issue-date`  | The lower secondary school degree issue date   | `Date \| string` | `"2003/12/30"`                 |
| `lowerSecondarySchoolGrade`            | `lower-secondary-school-grade`              | The lower secondary school degree issue grade  | `string`         | `"2010/04/30"`                 |
| `lowerSecondarySchoolName`             | `lower-secondary-school-name`               | The lower secondary school name                | `string`         | `"Lower Secondary School"`     |
| `nationality`                          | `nationality`                               | Nationality                                    | `string`         | `"Portugal"`                   |
| `primarySchoolDegreeIssueDate`         | `primary-school-degree-issue-date`          | The primary school degree issue date           | `Date \| string` | `"2003/12/30"`                 |
| `primarySchoolGrade`                   | `primary-school-grade`                      | The primary school degree issue date           | `string`         | `"4"`                          |
| `primarySchoolName`                    | `primary-school-name`                       | The primary school name                        | `string`         | `"Primary School"`             |
| `scrImage`                             | `src-image`                                 | Path to the image that will be embed           | `string`         | `"pt-ba.png"`                  |
| `surname`                              | `surname`                                   | The surname of the id                          | `string`         | `"dos Santos Barbosa"`         |
| `tertiaryDegreeIssueDate`              | `tertiary-degree-issue-date`                | The tertiary school degree issue date          | `Date \| string` | `"2003/12/30"`                 |
| `tertiarySchoolGrade`                  | `tertiary-school-grade`                     | The tertiary  school degree issue grade        | `string`         | `"6"`                          |
| `tertiarySchoolName`                   | `tertiary-school-name`                      | The tertiary school name                       | `string`         | `"Tertiary School"`            |
| `tertiarySchoolType`                   | `tertiary-school-type`                      | The tertiary  school degree issue grade        | `string`         | `"University"`                 |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
