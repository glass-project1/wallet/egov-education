import { newE2EPage } from '@stencil/core/testing';

describe('glass-education-records', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-education-records></glass-education-records>');

    const element = await page.find('glass-education-records');
    expect(element).toHaveClass('hydrated');
  });
});
