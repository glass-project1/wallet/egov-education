import { newE2EPage } from '@stencil/core/testing';

describe('glass-education-records-pt', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-education-records-pt></glass-education-records-pt>');

    const element = await page.find('glass-education-records-pt');
    expect(element).toHaveClass('hydrated');
  });
});
