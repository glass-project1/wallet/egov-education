import { Component, Host, Prop, getAssetPath, h } from '@stencil/core';

@Component({
  tag: 'glass-education-records-pt',
  styleUrl: 'glass-education-records-pt.scss',
  shadow: true,
})
export class GlassEducationRecordsPt {

  /**
   * The id-number of the id
   */
  @Prop({attribute: "id-number"}) idNumber: string = "GB 14 WXYZ 205623 25648978";

  /**
   * The givenname of the id
   */
  @Prop({attribute: "given-name"}) givenName: string = "Ana Cardoso";

  /**
   * The surname of the id
   */
  @Prop({attribute: "surname"}) surname: string = "dos Santos Barbosa";

  /**
  * Insurance Expiration date
  */
  @Prop({attribute: "birth-date"}) birthDate: Date | string = "1990/01/01";

  /**
  * Nationality
  */
  @Prop({attribute: "nationality"}) nationality: string = "Portugal"

  /**
  * The sex
  */
  @Prop({attribute: "gender"}) gender: string = "Female"

  /**
  * The primary school name
  */
  @Prop({attribute: "primary-school-name"}) primarySchoolName: string = "Primary School";

  /**
  * The primary school degree issue date
  */
  @Prop({attribute: "primary-school-degree-issue-date"}) primarySchoolDegreeIssueDate: Date | string = "2003/12/30";

  /**
  * The primary school degree issue date
  */
   @Prop({attribute: "primary-school-grade"}) primarySchoolGrade: string = "4";

  /**
  * The lower secondary school name
  */
  @Prop({attribute: "lower-secondary-school-name"}) lowerSecondarySchoolName: string = "Lower Secondary School";

  /**
  * The lower secondary school degree issue date
  */
  @Prop({attribute: "lower-secondary-school-degree-issue-date"}) lowerSecondarySchoolDegreeIssueDate: Date | string = "2003/12/30";

  /**
  * The lower secondary school degree issue grade
  */
  @Prop({attribute: "lower-secondary-school-grade"}) lowerSecondarySchoolGrade: string = "2010/04/30";

  /**
  * The higher secondary school name
  */
  @Prop({attribute: "higher-secondary-school-name"}) higherSecondarySchoolName: string = "2003/12/30";

  /**
  * The higher secondary school degree issue date
  */
  @Prop({attribute: "higher-secondary-school-degree-issue-date"}) higherSecondarySchoolDegreeIssueDate: Date | string = "2003/12/30";

  /**
  * The higher secondary school degree issue grade
  */
  @Prop({attribute: "higher-secondary-school-grade"}) higherSecondarySchoolGrade: string = "8";

  /**
  * The tertiary school name
  */
  @Prop({attribute: "tertiary-school-name"}) tertiarySchoolName: string = "Tertiary School";

  /**
  * The tertiary school degree issue date
  */
  @Prop({attribute: "tertiary-degree-issue-date"}) tertiaryDegreeIssueDate: Date | string = "2003/12/30";

  /**
  * The tertiary  school degree issue grade
  */
  @Prop({attribute: "tertiary-school-grade"}) tertiarySchoolGrade: string = "6";

  /**
  * The tertiary  school degree issue grade
  */
  @Prop({attribute: "tertiary-school-type"}) tertiarySchoolType: string = "University";

  /**
  * The tertiary  school degree issue grade
  */
  @Prop({attribute: "ects-credits"}) ectsCredits: string = "10";

  /**
   * Path to the image that will be embed
   */
  @Prop({attribute: "src-image"}) scrImage: string = "pt-ba.png";



  private parseDate(date: Date | string){
    return new Date(date).toLocaleString('pt-PT', {dateStyle: 'medium'});
  }

  private renderSchoolData(data: {title: string, name: string, degreeIssueDate: string | Date, grade?: string, type?: string}) {

    return [
      <ion-row class="g-item">
        <ion-col size="12">
          <h2>{data.title}</h2>
          <p>{data.name}</p>
        </ion-col>
        {!!data.type ?
          <ion-col size="12">
             <h2>Tipo</h2>
             <p>{data.type}</p>
          </ion-col> : ""
        }
        <ion-col size="6">
          <h2>Emissão do diploma em</h2>
          <p>{data.degreeIssueDate ? this.parseDate(data.degreeIssueDate) : data.degreeIssueDate}</p>
        </ion-col>
        <ion-col size="6">
          <h2>Grade</h2>
          <p>{data.grade}</p>
        </ion-col>
      </ion-row>
    ];
  }

  render() {
    const self = this;

    return (
        <div class="g-certificate">
          <ion-grid class="g-certificate-header">
            <ion-row>
              <ion-col>
                <ion-img src={getAssetPath(`../assets/images/${this.scrImage}`)}></ion-img>
                <ion-text>
                  <h1>Registros Educacionais</h1>
                  <p>Verifique seus dados escolares</p>
                </ion-text>
              </ion-col>
            </ion-row>
          </ion-grid>
          <ion-grid class="g-certificate-content">
            <ion-row>
              <ion-col size="6">
                <h2>Identidade</h2>
                <p>{self.idNumber}</p>
              </ion-col>
              <ion-col size="6">
                <h2>Data de Nascimento</h2>
                <p>{self.parseDate(self.birthDate)}</p>
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col size="6" size-sm="12">
                <h2>Nome</h2>
                <p>{self.givenName}</p>
              </ion-col>

              <ion-col size="6" size-sm="12">
                <h2>Sobrenome</h2>
                <p>{self.surname}</p>
              </ion-col>
            </ion-row>

            {self.primarySchoolName ? self.renderSchoolData({
              title: "Escola Primária",
              name: self.primarySchoolName,
              degreeIssueDate: self.primarySchoolDegreeIssueDate,
              grade: self.primarySchoolGrade
            }) : ""}

            {self.lowerSecondarySchoolName ? self.renderSchoolData({
              title: "Escola Secundária Inferior",
              name: self.lowerSecondarySchoolName,
              degreeIssueDate: self.lowerSecondarySchoolDegreeIssueDate,
              grade: self.lowerSecondarySchoolGrade
            }) : ""}

            {self.higherSecondarySchoolName ? self.renderSchoolData({
              title: "Escola Secundária superior",
              name: self.higherSecondarySchoolName,
              degreeIssueDate: self.higherSecondarySchoolDegreeIssueDate,
              grade: self.higherSecondarySchoolGrade
            }) : ""}

            {self.tertiarySchoolName ? self.renderSchoolData({
              title: "Escola Terciária",
              name: self.tertiarySchoolName,
              degreeIssueDate: self.tertiaryDegreeIssueDate,
              grade: self.tertiarySchoolGrade,
              type: self.tertiarySchoolType
            }) : ""}

            <ion-row>
              <ion-col>
                <h2>Créditos ECTS</h2>
                <p>{self.ectsCredits ? self.ectsCredits : '-'}</p>
              </ion-col>
            </ion-row>
          </ion-grid>
        </div>
    );
  }

}
