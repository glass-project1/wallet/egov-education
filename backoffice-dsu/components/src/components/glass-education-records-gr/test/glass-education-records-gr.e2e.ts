import { newE2EPage } from '@stencil/core/testing';

describe('glass-education-records-gr', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-education-records-gr></glass-education-records-gr>');

    const element = await page.find('glass-education-records-gr');
    expect(element).toHaveClass('hydrated');
  });
});
